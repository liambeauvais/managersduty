import kaboom from "kaboom";
import {loadSprites} from "./sprites";
import {loadScenes} from "./scenes/scenes";
import {getKaboomScale} from "./environment";

kaboom({
    scale: getKaboomScale(),
    background: [0, 0, 0],
})

loadSprites()
loadScenes()

function start(){
    go("office", 1)
}

start()
