export function loadSprites() {
    loadAlex();
    loadAdam();
    loadBob();
    loadAmelia();
    loadInteriors();
    loadRoomParts();
}

function loadBob() {
    loadSpriteAtlas('./sprites/characters/Bob_16x16.png', {
        "bob": {
            "x": 0,
            "y": 32,
            "width": 384,
            "height": 72,
            "sliceX": 24,
            "sliceY": 2,
            "anims": {
                "idle-right": {
                    "from": 0,
                    "to": 5,
                    "speed": 12,
                    "loop": true,
                },
                "idle-up": {
                    "from": 6,
                    "to": 11,
                    "speed": 12,
                    "loop": true,
                },
                "idle-left": {
                    "from": 12,
                    "to": 17,
                    "speed": 12,
                    "loop": true,
                },
                "idle-down": {
                    "from": 18,
                    "to": 23,
                    "speed": 12,
                    "loop": true,
                },
                "run-right": {
                    "from": 24,
                    "to": 29,
                    "speed": 12,
                    "loop": true,
                },
                "run-up": {
                    "from": 30,
                    "to": 35,
                    "speed": 12,
                    "loop": true,
                },
                "run-left": {
                    "from": 36,
                    "to": 41,
                    "speed": 12,
                    "loop": true,
                },
                "run-down": {
                    "from": 42,
                    "to": 47,
                    "speed": 12,
                    "loop": true,
                },
            }
        },
        "bob_phone":{
            "x":0,
            "y":196,
            "width":384,
            "height":36,
            "sliceX":24,
            "anims": {
                "phone_looking":{
                    "from":0,
                    "to": 8,
                    "speed": 12,
                    "loop": true,
                }
            }
        }
    })
}

function loadAlex() {
    loadSpriteAtlas('./sprites/characters/Alex_16x16.png', {
        'alex': {
            x: 0,
            y: 32,
            width: 384,
            height: 72,
            sliceX: 24,
            sliceY:2,
            anims: {
                "idle-right": {
                    from: 0,
                    to: 5,
                    speed: 12,
                    loop: true
                },
                "idle-up": {
                    from: 6,
                    to: 11,
                    speed: 12,
                    loop: true
                },
                "idle-left": {
                    from: 12,
                    to: 17,
                    speed: 12,
                    loop: true
                },
                "idle-down": {
                    from: 18,
                    to: 23,
                    speed: 12,
                    loop: true
                },
                "run-right": {
                    from: 24,
                    to: 29,
                    speed: 12,
                    loop: true
                },
                "run-up": {
                    from: 30,
                    to: 35,
                    speed: 12,
                    loop: true
                },
                "run-left": {
                    from: 36,
                    to: 41,
                    speed: 12,
                    loop: true
                },
                "run-down": {
                    from: 42,
                    to: 47,
                    speed: 12,
                    loop: true
                },
            }
        },
    })
}

function loadAdam() {
    loadSpriteAtlas('./sprites/characters/Adam_16x16.png', {
        "adam": {
            "x": 0,
            "y": 32,
            "width": 384,
            "height": 72,
            "sliceX": 24,
            sliceY:2,
            "anims": {
                "idle-right": {
                    "from": 0,
                    "to": 5,
                    "speed": 12,
                    "loop": true,
                },
                "idle-up": {
                    "from": 6,
                    "to": 11,
                    "speed": 12,
                    "loop": true,
                },
                "idle-left": {
                    "from": 12,
                    "to": 17,
                    "speed": 12,
                    "loop": true,
                },
                "idle-down": {
                    "from": 18,
                    "to": 23,
                    "speed": 12,
                    "loop": true,
                },
                "run-right": {
                    "from": 24,
                    "to": 29,
                    "speed": 12,
                    "loop": true,
                },
                "run-up": {
                    "from": 30,
                    "to": 35,
                    "speed": 12,
                    "loop": true,
                },
                "run-left": {
                    "from": 36,
                    "to": 41,
                    "speed": 12,
                    "loop": true,
                },
                "run-down": {
                    "from": 42,
                    "to": 47,
                    "speed": 12,
                    "loop": true,
                },
            }
        },
        "adam-run": {
            "x": 0,
            "y": 68,
            "width": 384,
            "height": 36,
            "sliceX": 24,
            "anims": {
                "run-right": {
                    "from": 0,
                    "to": 5,
                    "speed": 12,
                    "loop": true,
                },
                "run-up": {
                    "from": 6,
                    "to": 11,
                    "speed": 12,
                    "loop": true,
                },
                "run-left": {
                    "from": 12,
                    "to": 17,
                    "speed": 12,
                    "loop": true,
                },
                "run-down": {
                    "from": 18,
                    "to": 23,
                    "speed": 12,
                    "loop": true,
                },
            }
        }
    })
}

function loadAmelia(){
    loadSpriteAtlas('./sprites/characters/Amelia_16x16.png', {
        "amelia": {
            "x": 0,
            "y": 32,
            "width": 384,
            "height": 72,
            "sliceX": 24,
            "sliceY": 2,
            "anims": {
                "idle-right": {
                    "from": 0,
                    "to": 5,
                    "speed": 12,
                    "loop": true,
                },
                "idle-up": {
                    "from": 6,
                    "to": 11,
                    "speed": 12,
                    "loop": true,
                },
                "idle-left": {
                    "from": 12,
                    "to": 17,
                    "speed": 12,
                    "loop": true,
                },
                "idle-down": {
                    "from": 18,
                    "to": 23,
                    "speed": 12,
                    "loop": true,
                },
                "run-right": {
                    "from": 24,
                    "to": 29,
                    "speed": 12,
                    "loop": true,
                },
                "run-up": {
                    "from": 30,
                    "to": 35,
                    "speed": 12,
                    "loop": true,
                },
                "run-left": {
                    "from": 36,
                    "to": 41,
                    "speed": 12,
                    "loop": true,
                },
                "run-down": {
                    "from": 42,
                    "to": 47,
                    "speed": 12,
                    "loop": true,
                },
            }
        }
    })
}

function loadInteriors() {
    loadSpriteAtlas('./sprites/PixelOfficeAssets.png',{
        'table':{x:84,y:40,width:30,height:24},
        'chair-left':{x:6,y:40,width:12,height:24},
        'flowers':{x:170,y:64,width:16,height:20},
        'coffe':{x:158,y:122,width:26,height:42}
    })
}

function loadRoomParts() {
    loadSpriteAtlas('./sprites/Room_Builder.png', {
        'floor': {x: 350, y: 416, width: 48, height: 48},
        'wall-up': {x: 0, y: 150, width: 32, height: 48},
        'wall-side': {x: 96, y: 150, width: 32, height: 48},
    })
}
