let score = 0

export function getScore(){
    return score
}

export function setScore(modifiedScore){
    score = modifiedScore
    return score
}

let kaboomScale = 4

export function getKaboomScale(){
    return kaboomScale
}