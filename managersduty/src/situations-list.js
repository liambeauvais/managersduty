import {Situation, Dialog} from "./models";

const situations = [
    new Situation(
        1,
        [
            new Dialog(
                "adam",
                "idle-down",
                "Chantal!"
            ),
            new Dialog(
                "adam",
                "idle-down",
                "Vous pouvez retirer votre pied de mon entrejambe?"
            )
        ],
        "Placer son pied au niveau de l'entrejambe d'un collègue est totalement inapproprié en milieu professionnel. Cela constitue une agression sexuelle, une violation de l'intimité personnelle et un manque de respect envers les autres. Il est impératif de créer un environnement de travail sûr, respectueux et professionnel pour tous. Le respect des limites personnelles est essentiel pour établir des relations de travail saines et éthiques.",
        ["xxxxxxxxxx",
            "y      c y",
            "y        y",
            "y   a   dy",
            "y   t    y",
            "y   b    y",
            "y        y",
            "y     p  y",
            "y f      y",
            "xxxxxxxxxx"],
        2
    )
    , new Situation(
        2,
        [
            new Dialog(
                "adam",
                "idle-down",
                "Tu sais, si tu veux monter en grade ici, il y a certaines choses que tu pourrais faire pour m'aider..."
            ),
            new Dialog(
                "alex",
                "idle-down",
                "Je suis ici pour mon travail, pas pour ça."
            ),
        ],
        "Le harcèlement sexuel au travail est inacceptable. Si vous êtes confronté à une telle situation, signalez-la à votre supérieur hiérarchique ou au service des ressources humaines.",
        ["xxxxxxxxxx",
            "y c      y",
            "y    rl  y",
            "y        y",
            "y        y",
            "y        y",
            "y    t   y",
            "y        y",
            "y f      y",
            "xxxxxxxxxx"],
        1
    ),
    new Situation(
        3,
        [
            new Dialog(
                "adam",
                "idle-down",
                "(touche les fesses d'Amelia)"
            ),
            new Dialog(
                "alex",
                "idle-down",
                "Non, je ne suis pas à l'aise avec ça. Ne fais plus jamais ça"
            ),
        ],
        "Un contact déplacé peut-être considéré comme une agression sexuelle." +
        "L'agression sexuelle au travail a des conséquences dévastatrices sur la vie des victimes, qu'il s'agisse d'hommes ou de femmes. Il est essentiel de sensibiliser à ce problème, de mettre en place des politiques de prévention et de soutien pour les victimes, et d'encourager une culture de travail respectueuse et sûre pour tous.",
        ["xxxxxxxxxx",
            "y f    c y",
            "y    rl  y",
            "y        y",
            "y        y",
            "y        y",
            "y        y",
            "y     p  y",
            "y        y",
            "xxxxxxxxxx"], 2
    ),
    new Situation(
        4,
        [
            new Dialog(
                "adam",
                "idle-down",
                "(montre des images à caractère sexuel)"
            ),
            new Dialog(
            "adam",
            "idle-down",
            "Tu as l'air d'aimer ça. Pourquoi ne pas passer chez moi ce soir ?"
            ),
            new Dialog(
                "alex",
                "idle-down",
                "Je n'ai aucune envie de continuer cette conversation."
            ),
        ],
        "Il est essentiel de fixer des limites claires lorsque vous vous sentez harcelé. Vous avez le droit de dire non et de vous éloigner de toute situation qui vous met mal à l'aise.",
        ["xxxxxxxxxx",
            "y c      y",
            "y    rl  y",
            "y        y",
            "y        y",
            "y        y",
            "y        y",
            "y      t y",
            "y f      y",
            "xxxxxxxxxx"],
        1
    ),
    new Situation(
        5,
        [
            new Dialog(
                "adam",
                "idle-down",
                "Salut Amelia, jolie ta jupe"
            ),
            new Dialog(
                "amelia",
                "idle-down",
                "merci..."
            ),
            new Dialog(
                "adam",
                "idle-down",
                "Salut Amelia, ça te dirait un déjeuner 'professionel'?"
            ),
            new Dialog(
                "amelia",
                "idle-down",
                "Pour la deuxième fois, non merci.."
            ), new Dialog(
            "adam",
            "idle-down",
            "Eh fais pas ta mijorée là"
        ),
            new Dialog(
                "amelia",
                "idle-down",
                "..."
            ),
        ],
        "Amelia a refusé plusieures fois les avances de Adam, de plus en plus insistant." +
        "Une des caractéristiques définissant le harcèlement sexuel est la répétition.",
        ["xxxxxxxxxx",
            "y c      y",
            "y      f y",
            "y        y",
            "y        y",
            "y        y",
            "y        y",
            "y  rl    y",
            "y        y",
            "xxxxxxxxxx"],
        1
    )
]

export function getSituationsNumber() {
    return situations.length;
}

export function getSituationById(id) {
    let targetedSituation = null
    situations.map(
        (situation) => {
            if (situation.id === id) {
                targetedSituation = situation
            }
        }
    )
    return targetedSituation
}
