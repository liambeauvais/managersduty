export class Situation {

    constructor(id,dialogs,sensitization, scene, goodAnswer) {
        this.id = id;
        this.dialogs = dialogs;
        this.sensitization = sensitization;
        this.scene = scene;
        this.goodAnswer = goodAnswer
    }
}
