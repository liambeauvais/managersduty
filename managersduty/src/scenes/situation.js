import {getSituationById} from "../situations-list";
import {getKaboomScale} from "../environment";

export function loadSituationScene() {
    scene('situation', (index) => {
        const situation = getSituationById(index)
        const dialogs = situation.dialogs

        let curDialog = 0

        // Text bubble
        const textbox = add([
            rect(width()/2, height()/3, {radius: 32}),
            anchor("center"),
            pos(center().x, height() - 80),
            outline(4),
        ])

        // Text
        const txt = add([
            text("", {size: 10, width: 2*width()/5, align: "center"}),
            pos(textbox.pos),
            anchor("center"),
            color(0, 0, 0),
        ])

        // Character avatar
        const avatar = add([
            sprite("adam", {anim: "idle-down"}),
            scale(
                (height()*getKaboomScale())
                /(1920/3)
            ),
            anchor("center"),
            pos(center().sub(0, 50)),
        ])

        onKeyPress("space", () => {
            // Cycle through the dialogs
            curDialog = (curDialog + 1)
            if (curDialog >= dialogs.length) {
                go("question", index)
            } else {
                updateDialog()
            }
        })

        // Update the on screen sprite & text
        function updateDialog() {

            const dialog = dialogs[curDialog]

            // Use a new sprite component to replace the old one
            avatar.use(sprite(dialog.sprite, {anim: dialog.anim}))
            // Update the dialog text
            txt.text = `${dialog.sprite}: ${dialog.text}`

        }

        updateDialog()

    })
}