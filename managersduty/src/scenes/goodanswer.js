export function loadGoodAnswerScene(){
    scene('goodanswer', (index) => {

// Text
        add([
            text("Bonne réponse!", { width: width(), align: "center" }),
            pos(center()),
            anchor("center"),
            color(255, 255, 255),
        ])

        onKeyPress("space", () => {
            go('sensitisation', index)
        })

    })
}