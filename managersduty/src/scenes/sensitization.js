import {getSituationById, getSituationsNumber} from "../situations-list";

export function loadSensitizationScene() {
    scene('sensitisation', (index) => {
        const situation = getSituationById(index)
        const sensitization = situation.sensitization

// Text
        add([
            text(sensitization, { size: 12, width: 2*width()/3, align: "center" }),
            pos(center()),
            anchor("center"),
            color(255, 255, 255),
        ])

        onKeyPress("space", () => {
            if (index >= getSituationsNumber()){
                go("end")
            }
            else{
                go('office', index+1)
            }
        })

    })
}