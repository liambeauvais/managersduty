import {getSituationById} from "../situations-list";
import {getScore, setScore} from "../environment";

export function loadQuestionScene(){
    scene('question',(index)=>{
        onUpdate(() => setCursor("default"))

        function addButton(txt, p, f) {

            // add a parent background object
            const btn = add([
                rect(width()/2, height()/6, { radius: 8 }),
                pos(p),
                area(),
                scale(1),
                anchor("center"),
                outline(4),
            ])

            // add a child object that displays the text
            btn.add([
                text(txt, {size:14}),
                anchor("center"),
                color(0, 0, 0),
            ])

            // onHoverUpdate() comes from area() component
            // it runs every frame when the object is being hovered
            btn.onHoverUpdate(() => {
                const t = time() * 10
                btn.color = hsl2rgb((t / 10) % 1, 0.6, 0.7)
                btn.scale = vec2(1.2)
                setCursor("pointer")
            })

            // onHoverEnd() comes from area() component
            // it runs once when the object stopped being hovered
            btn.onHoverEnd(() => {
                btn.scale = vec2(1)
                btn.color = rgb()
            })

            // onClick() comes from area() component
            // it runs once when the object is clicked
            btn.onClick(f)

            return btn

        }
        addButton("Drague", vec2(width()/2, height()/8), () => checkResponse(index, 0))
        addButton("Harcèlement sexuel", vec2(width()/2, 3*height()/8), () => checkResponse(index, 1))
        addButton("Agression sexuelle", vec2(width()/2, 5*height()/8), () => checkResponse(index, 2))
        addButton("Rien de cela ", vec2(width()/2, 7*height()/8), () => checkResponse(index, 3))

        function checkResponse(index, indexButton){
            const situation = getSituationById(index)
            if(situation.goodAnswer === indexButton){
                let score = getScore()
                score ++;
                setScore(score)
                go("goodanswer", index)
            }
            else{
                go("badanswer", index)
            }
        }

    })
}