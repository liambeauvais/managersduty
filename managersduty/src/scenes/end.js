import {getScore, setScore} from "../environment";
import {getSituationsNumber} from "../situations-list";

export function loadEndScene() {
    scene('end', () => {

        // Text
        add([
            text("Press space to retry", {width: width(), size: 14}),
            pos(12, 12),
        ])

        add([
            text("FIN ...", {width: width(), align: "center"}),
            pos(center()),
            anchor("center"),
            color(255, 255, 255),
        ])

        add([
            text(`Bonnes réponses: ${getScore()}/${getSituationsNumber()}`, {width: width(),size:14, align: "center"}),
            pos(width() / 2, height() - 20),
            anchor("center"),
            color(255, 255, 255),
        ])

        onKeyPress("space", () => {
            setScore(0)
            go('office', 1)
        })

    })
}