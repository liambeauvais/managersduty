export function loadBadAnswerScene(){
    scene('badanswer', (index) => {

// Text
        add([
            text("Mauvaise réponse!", { width: width(), align: "center" }),
            pos(center()),
            anchor("center"),
            color(255, 255, 255),
        ])

        onKeyPress("space", () => {
            go('sensitisation', index)
        })

    })
}