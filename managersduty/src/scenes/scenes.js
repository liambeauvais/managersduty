import {loadOfficeScene} from "./office";
import {loadSituationScene} from "./situation";
import {loadSensitizationScene} from "./sensitization";
import {loadQuestionScene} from "./question";
import {loadGoodAnswerScene} from "./goodanswer";
import {loadBadAnswerScene} from "./badanswer";
import {loadEndScene} from "./end";

export function loadScenes(){
    loadOfficeScene()
    loadSituationScene()
    loadSensitizationScene()
    loadQuestionScene()
    loadGoodAnswerScene()
    loadBadAnswerScene()
    loadEndScene()
}