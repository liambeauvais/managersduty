import {getSituationById} from "../situations-list";

export function loadOfficeScene(){
    scene('office', (index) => {
        addLevel([
            "xxxxxxxxxx",
            "x        x",
            "x        x",
            "x        x",
            "x        x",
            "x        x",
            "x        x",
            "x        x",
            "x        x",
            "xxxxxxxxxx",
        ], {
            tileWidth: 16,
            tileHeight: 16  ,
            tiles: {
                " ": () => [
                    sprite("floor"), scale(0.5)
                ],
            },
        })

        const map = addLevel(getSituationById(index).scene, {
            // define the size of tile block
            tileWidth: 16,
            tileHeight: 16,
            // define what each symbol means, by a function returning a component list (what will be passed to add())
            tiles: {
                "x": () => [
                    sprite("wall-up"),
                    scale(0.5),
                    area(),
                    body({ isStatic: true }),
                    tile({ isObstacle: true }),
                ],
                "y": () => [
                    sprite("wall-side"),
                    scale(0.5),
                    area(),
                    body({ isStatic: true }),
                    tile({ isObstacle: true }),
                ],
                "t": () => [
                    sprite("table"),
                    scale(1),
                    area(),
                    anchor("center"),
                    body({ isStatic: true }),
                    tile({ isObstacle: true }),
                ],
                "c": () => [
                    sprite("coffe"),
                    scale(1),
                    area(),
                    body({ isStatic: true }),
                    tile({ isObstacle: true }),
                ],
                "a": () => [
                    sprite("amelia",{anim:"idle-down"}),
                    anchor("center"),
                    "amelia",
                    area({ scale: 0.5 }),
                    body({ isStatic: true }),
                    tile({ isObstacle: true }),
                ],
                "b": () => [
                    sprite("adam",{anim:"idle-up"}),
                    anchor("center"),
                    "adam",
                    area({ scale: 0.5 }),
                    body({ isStatic: true }),
                    tile({ isObstacle: true }),
                ],
                "p": () => [
                    sprite("bob_phone",{anim:"phone_looking"}),
                    anchor("center"),
                    "bob",
                    area({ scale: 0.5 }),
                    body({ isStatic: true }),
                    tile({ isObstacle: true }),
                ],
                "r": () => [
                    sprite("adam",{anim:"idle-right"}),
                    anchor("center"),
                    "adam",
                    area({ scale: 0.5 }),
                    body({ isStatic: true }),
                    tile({ isObstacle: true }),
                ],
                "l": () => [
                    sprite("alex",{anim:"idle-left"}),
                    anchor("center"),
                    "alex",
                    area({ scale: 0.5 }),
                    body({ isStatic: true }),
                    tile({ isObstacle: true }),
                ],
                "d": () => [
                    sprite("alex",{anim:"idle-up"}),
                    anchor("center"),
                    "alex",
                    area({ scale: 0.5 }),
                    body({ isStatic: true }),
                    tile({ isObstacle: true }),
                ],
                "f": () => [
                    sprite("flowers"),
                    scale(1),
                    area(),
                    anchor("center"),
                    body({ isStatic: true }),
                    tile({ isObstacle: true }),
                ],
            }
        })




        const player = map.spawn([
            sprite("bob", { anim: "idle-right" }),
            area({ shape: new Rect(vec2(0, 6), 12, 12) }),
            body(),
            anchor("center"),
            tile(),
        ], 2, 2)

        player.onCollide("adam", () => {
            go("situation",index );
        })

        player.onCollide("alex", () => {
            go("situation",index );
        })

        player.onCollide("amelia", () => {
            go("situation",index );
        })

        const SPEED = 120

        player.onUpdate(() => {
            camPos(player.pos)
        })

        player.onPhysicsResolve(() => {
            // Set the viewport center to player.pos
            camPos(player.pos)
        })

        onKeyDown("right", () => {
            player.move(SPEED, 0)
        })

        onKeyDown("left", () => {
            player.move(-SPEED, 0)
        })

        onKeyDown("up", () => {
            player.move(0, -SPEED)
        })

        onKeyDown("down", () => {
            player.move(0, SPEED)
        });

        ["left", "right", "up", "down"].forEach((key) => {
            onKeyPress(key, () => {
                player.play(`run-${key}`)
            })
            onKeyRelease(key, () => {
                if (
                    !isKeyDown("left")
                    && !isKeyDown("right")
                    && !isKeyDown("up")
                    && !isKeyDown("down")
                ) {
                    player.play(`idle-${key}`)
                }
            })
        })
    })
}
